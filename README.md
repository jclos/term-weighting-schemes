# A collection of term weighting schemes for information retrieval and supervised machine learning #

This project contains different term weighting schemes, both for unsupervised term weighting (in the case of information retrieval) or supervised term weighting (basing the weights on classes). The code is **not** optimized at all and is released under a [CRAPL license](http://matt.might.net/articles/crapl/).

### What is this repository for? ###

This repository contains two packages:
* The rgu.ac.uk.tfidf.basic package contains the unsupervised weighting schemes.
* rgu.ac.uk.tfidf.classification package contains the supervised weighting schemes. They require prior knowledge of which class each instance belongs to.


### How do I use the code? ###

* For the unsupervised term weighting package, you will have to create a DocumentBase object and add Document objects to it
* For the supervised term weighting package, you will have to create a DocumentBase object and add Document objects to it along with the label of their class
* In both cases, you can either query the TF-IDF straight away with DocumentBase.tfIdf or use the DocumentBase.getValue function to change the term weighting and normalization factors
* The following schemes are available in the unsupervised package:
     * **Term Weighting**: Boolean, NaturalFrequency, LogFrequency, AugmentedFrequency, LogAverageFrequency, BM25Frequency
     * **Normalization Factor**: NoNormalization, IDF, ProbabilisticIDF, BM25IDF
* The following schemes are available in the supervised package:
     * **Term Weighting**: Boolean, NaturalFrequency, LogFrequency, AugmentedFrequency, LogAverageFrequency, BM25Frequency
     * **Normalization Factor**: NoNormalization, IDF, ProbabilisticIDF, BM25IDF, DeltaIDF, DeltaSmoothedIDF, DeltaProbIDF, DeltaSmoothedProbIDF, DeltaBM25IDF

Each package contains a small Test.java file which contains some example code which illustrates the use of the library. They are copied below for convenience sake.


#### Unsupervised Term Weighting
```
#!java
public class Test {
    public static void main(String[] args) {
        Document product1 = new Document("Product1");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("size");
        product1.addTerm("delivery");
        product1.addTerm("screen");
        Document product2 = new Document("Product2");
        product2.addTerm("price");
        product2.addTerm("delivery");
        Document product3 = new Document("Product3");
        product3.addTerm("price");
        product3.addTerm("delivery");
        Document product4 = new Document("Product4");
        product4.addTerm("screen");
        product4.addTerm("price");
        Document product5 = new Document("Product5");
        product5.addTerm("price");
        
        DocumentBase registry = new DocumentBase();
        registry.addDocument(product1);
        registry.addDocument(product2);
        registry.addDocument(product3);
        registry.addDocument(product4);
        registry.addDocument(product5);
        
        System.out.println(DocumentBase.tfIdf("price", product1, registry));
        
    }
}
```

####Supervised Term Weighting

```
#!java
public class Test {
    public static void main(String[] args) {
        Document product1 = new Document("Product1","positive");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("size");
        product1.addTerm("delivery");
        product1.addTerm("screen");
        product1.addTerm("good");
        product1.addTerm("amazing");
        product1.addTerm("ok");
        Document product2 = new Document("Product2","negative");
        product2.addTerm("price");
        product2.addTerm("delivery");
        product2.addTerm("terrible");
        product2.addTerm("bad");
        product2.addTerm("sad");
        Document product3 = new Document("Product3","positive");
        product3.addTerm("price");
        product3.addTerm("good");
        product3.addTerm("like");
        product3.addTerm("delivery");
        Document product4 = new Document("Product4","positive");
        product4.addTerm("screen");
        product4.addTerm("good");
        product4.addTerm("wellmade");
        product4.addTerm("excellent");
        Document product5 = new Document("Product5","negative");
        product5.addTerm("terrible");
        product5.addTerm("bad");
        product5.addTerm("disagree");
        product5.addTerm("dangerous");
        
        DocumentBase registry = new DocumentBase();
        registry.addDocument(product1);
        registry.addDocument(product2);
        registry.addDocument(product3);
        registry.addDocument(product4);
        registry.addDocument(product5);
     
        registry.computeLabels();
        System.out.println(DocumentBase.getValue("screen", product1, registry, TermWeight.LogFrequency, NormFactor.ProbabilisticIDF));
        System.out.println(DocumentBase.tfIdf("screen", product1, registry));
        
    }
}

```
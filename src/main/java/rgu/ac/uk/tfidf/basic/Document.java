/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.tfidf.basic;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Document {
    private final Map<String, Integer> _internal ;
    private final String _docId ;
    
    public Document(String docId){
        _internal = new HashMap<>();
        _docId = docId ;
    }
    
    public String getId() {
        return _docId ;
    }
    
    public Set<Map.Entry<String, Integer>> entrySet() {
        return _internal.entrySet();
    }
    
    public Set<String> allTerms() {
        return _internal.keySet();
    }
    
    public void addTerm(String term) {
        _internal.put(term, _internal.getOrDefault(term, 0) + 1);
    }
    
    public void setTerm(String term, int frequency) {
        _internal.put(term, frequency);
    }
    
    private double averageTF() {
        int sum = 0 ;
        int count = 0 ;
        for (Map.Entry<String, Integer> term : _internal.entrySet()) {
            sum += term.getValue() ;
            count ++ ;
        }
        return (double) sum / (double) count ;
    }
    
    public int rawFrequency(String term) {
        return _internal.getOrDefault(term, 0);
    }
    
    public double augFrequency(String term) {
        int rawFreq = rawFrequency(term);
        int maxFreq = rawFreq ;
        for (Map.Entry<String, Integer> t : _internal.entrySet()) {
            if (t.getValue() > maxFreq) {
                maxFreq = t.getValue() ;
            }
        }
        return 0.5 + ((0.5*(double)rawFreq)/(double)maxFreq) ;
    }
    
    public double logFrequency(String term) {
        return log2(1 + _internal.getOrDefault(term, 0));
    }
    
    public double logAverageFrequency(String term) {
        double numerator = logFrequency(term);
        double denominator = log2(averageTF());
        return numerator / denominator ;
    }
    
    public int docLength() {
        int d = 0 ;
        for (Map.Entry<String, Integer> t : _internal.entrySet()) {
            d += t.getValue() ;
        }
        return d ;
    }
    
    public double BM25TF(String term, double k1, double b, double avgDL) {
        int f = rawFrequency(term);
        int d = docLength(); 
        double numerator = (k1 + 1) * f ;
        double denominator = k1 * ((1 - b) + b * ((double)d/avgDL)) + f ;
        return numerator / denominator ;
    }
    
    public static double log2(double v)
    {
        return Math.log10(v);
   //     return Math.log(v)/Math.log(2);
    }
}

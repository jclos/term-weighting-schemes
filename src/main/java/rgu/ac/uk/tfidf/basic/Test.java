/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.tfidf.basic;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Test {
    public static void main(String[] args) {
        Document product1 = new Document("Product1");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("size");
        product1.addTerm("delivery");
        product1.addTerm("screen");
        Document product2 = new Document("Product2");
        product2.addTerm("price");
        product2.addTerm("delivery");
        Document product3 = new Document("Product3");
        product3.addTerm("price");
        product3.addTerm("delivery");
        Document product4 = new Document("Product4");
        product4.addTerm("screen");
        product4.addTerm("price");
        Document product5 = new Document("Product5");
        product5.addTerm("price");
        
        DocumentBase registry = new DocumentBase();
        registry.addDocument(product1);
        registry.addDocument(product2);
        registry.addDocument(product3);
        registry.addDocument(product4);
        registry.addDocument(product5);
        
        System.out.println(DocumentBase.tfIdf("price", product1, registry));
        
    }
}

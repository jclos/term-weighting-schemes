/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.tfidf.basic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class DocumentBase {
    private final Map<String, Set<String>> _termToDocs ;
    private final Set<Document> _docs ;
   
    public DocumentBase() {
        _termToDocs = new HashMap<>();
        _docs = new HashSet<>();
    }
    
    private void addEntry(String term, String docId) {
        if (_termToDocs.containsKey(term)) {
            _termToDocs.get(term).add(docId);
        } else {
            Set<String> docs = new HashSet<>();
            docs.add(docId);
            _termToDocs.put(term, docs);
        }
    }
    
    public void addDocument(Document document) {
        if (!_docs.contains(document)) {
            _docs.add(document);
            document.allTerms().stream().forEach((term) -> {
                this.addEntry(term, document.getId());
            });
            
        }
    }
    
    public double getIdf(String term) {
        int bigN = _docs.size() ;
        int smallN = _termToDocs.getOrDefault(term, new HashSet<>()).size() ;
        double idf = smallN == 0 ? 0 : Document.log2((double)bigN/ (double)smallN );
        return idf;
    }
    
    public double getProbabilisticIdf(String term) {
        int bigN = _docs.size() ;
        int smallN = _termToDocs.getOrDefault(term, new HashSet<>()).size() ;
        double idf = smallN == 0 ? 0 : Document.log2((((double)bigN - (double)smallN)/ (double)smallN) );
        return idf;
    }
    
    public double getBM25Idf(String term) {
        int bigN = _docs.size() ;
        int smallN = _termToDocs.getOrDefault(term, new HashSet<>()).size() ;
        double idf = smallN == 0 ? 0 : Document.log2((((double)bigN - (double)smallN + 0.5)/ (double)smallN + 0.5) );
        return idf;
    }
    
    public static double tfIdf(String term, Document document, DocumentBase registry) {
        System.out.println("TF of " + term + " in " + document.getId() + " is " + document.logFrequency(term));
        System.out.println("IDF of " + term + " is " + registry.getIdf(term));
        double tfidf = document.logFrequency(term) * registry.getIdf(term) ;
        System.out.println("Therefore TFIDF(" + term + "," + document.getId()+") = " + tfidf);
        return tfidf;
    }
    public enum TermWeight {Boolean, NaturalFrequency, LogFrequency, AugmentedFrequency, LogAverageFrequency, BM25Frequency}
    public enum NormFactor {NoNormalization, IDF, ProbabilisticIDF, BM25IDF}
  
    public static double BM25_k1 = 1.2d;
    public static double BM25_b = 0.95d;
    public static double BM25_avgDL = -1d;
    
    public void computeBM25Values() {
        int totalLength = 0 ;
        for (Document doc : _docs) {
            totalLength += doc.docLength();
        }
        BM25_avgDL = (double) totalLength / (double) _docs.size() ;
    }
 
    public static double getValue(String term, Document document, DocumentBase registry, TermWeight tw, NormFactor nf) {
        double termWeight = 0d;
        double normFactor = 0d;
        NormFactor nf2 ;
        if (tw == TermWeight.Boolean) nf2 = NormFactor.NoNormalization ;
        else nf2 = nf ;
        //Term Weight
        switch(tw) {
            case Boolean:{
                if (document.rawFrequency(term) > 0) termWeight = 1 ;
                else termWeight = 0 ;
                break;
            }
            case NaturalFrequency: {
                termWeight = document.rawFrequency(term);
                break;
            }
            case LogFrequency: {
                termWeight = document.logFrequency(term);
                break;
            }
            case AugmentedFrequency: {
                termWeight = document.augFrequency(term);
                break;
            }
            case LogAverageFrequency: {
                termWeight = document.logAverageFrequency(term);
                break;
            }
            case BM25Frequency: {
                if (BM25_avgDL == -1d) registry.computeBM25Values();
                termWeight = document.BM25TF(term, BM25_k1, BM25_b, BM25_avgDL);
                break;
            }
        }
        //Normalization Factor
        switch (nf2) {
            case NoNormalization: {
                normFactor = 1 ;
                break;
            }
            case IDF: {
                normFactor = registry.getIdf(term);
                break;
            }
            case ProbabilisticIDF: {
                normFactor = registry.getProbabilisticIdf(term);
                break;
            }
            case BM25IDF: {
                normFactor = registry.getBM25Idf(term);
                break;
            }
        }
        return termWeight * normFactor;
    }
}

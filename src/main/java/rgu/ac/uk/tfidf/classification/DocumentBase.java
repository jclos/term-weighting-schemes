/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.tfidf.classification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class DocumentBase {
    private final Map<String, Set<String>> _termToDocs ;
    private final Map<String, Document> _docs ;
   
    public DocumentBase() {
        _termToDocs = new HashMap<>();
        _docs = new HashMap<>();
    }
    
    private void addEntry(String term, String docId) {
        if (_termToDocs.containsKey(term)) {
            _termToDocs.get(term).add(docId);
        } else {
            Set<String> docs = new HashSet<>();
            docs.add(docId);
            _termToDocs.put(term, docs);
        }
    }
    
    public void addDocument(Document document) {
        if (!_docs.values().contains(document)) {
            _docs.put(document.getId(), document);
            document.allTerms().stream().forEach((term) -> {
                this.addEntry(term, document.getId());
            });
        }
    }
    
    private List<String> _labels ;
    private Map<String, Integer> _labelCounts ;
    private Map<String, Double> _labelProportions ;
    
    public void computeLabels() {
        _labels = new ArrayList<>();
        _labelCounts = new HashMap<>();
        _labelProportions = new HashMap<>();
        for (Document doc : _docs.values()) {
            _labelCounts.put(doc.getLabel(), _labelCounts.getOrDefault(doc.getLabel(), 0) + 1);
        }
        for (Map.Entry<String, Integer> label : _labelCounts.entrySet())
        {
            _labelProportions.put(label.getKey(), (double) label.getValue() / _docs.size());
            _labels.add(label.getKey());
        }
        
    }
    
    public Map<String, Integer> getDFij(String term) {
        Map<String, Integer> dfij = new HashMap<>();
        List<String> docIds = new ArrayList<>();
        for (String doc : _termToDocs.getOrDefault(term, new HashSet<>())) {
            docIds.add(doc);
        }
        for (String docId : docIds) {
            Document doc = _docs.get(docId);
            if (doc.rawFrequency(term) > 0) {
                String _class = doc.getLabel();
                dfij.put(_class, dfij.getOrDefault(_class, 0) + 1);
            }
        }
        return dfij ;
    }
    
    public double getDeltaIdf(String term) {
        double result = 0d;
        double bigN1, bigN2, df1, df2 ;
        if (_labels.size() != 2) result = -1d; 
        else {
            String class1 = _labels.get(0);
            String class2 = _labels.get(1);
            Map<String, Integer> dfij = getDFij(term);
            df1 = dfij.getOrDefault(class1, 0);
            df2 = dfij.getOrDefault(class2, 0);
            bigN1 = _labelCounts.getOrDefault(class1,0);
            bigN2 = _labelCounts.getOrDefault(class2,0);
            result = ((double)df2 * (double)bigN1) / ((double)df1 * (double)bigN2) ;
        }
        return Document.log2(result) ;
    }
    
    public double getDeltaSmoothedIdf(String term) {
        double result = 0d;
        double bigN1, bigN2, df1, df2 ;
        if (_labels.size() != 2) result = -1d; 
        else {
            String class1 = _labels.get(0);
            String class2 = _labels.get(1);
            Map<String, Integer> dfij = getDFij(term);
            df1= dfij.get(class1);
            df2 = dfij.get(class2);
            bigN1 = _labelCounts.get(df1);
            bigN2 = _labelCounts.get(df2);
            result = ((double)df2 * (double)bigN1 + 0.5) / ((double)df1 * (double)bigN2 + 0.5) ;
        }
        return Document.log2(result) ;
    }
    
    
    public double getDeltaProbIdf(String term) {
        double result = 0d;
        double bigN1, bigN2, df1, df2 ;
        if (_labels.size() != 2) result = -1d; 
        else {
            String class1 = _labels.get(0);
            String class2 = _labels.get(1);
            Map<String, Integer> dfij = getDFij(term);
            df1= dfij.get(class1);
            df2 = dfij.get(class2);
            bigN1 = _labelCounts.get(df1);
            bigN2 = _labelCounts.get(df2);
            result = ((double)df2 * ((double)bigN1 - (double)df1)) / ((double)df1 * ((double)bigN2 - (double)df2)) ;
        }
        return Document.log2(result) ;
    }
    
    public double getDeltaSmoothedProbIdf(String term) {
        double result = 0d;
        double bigN1, bigN2, df1, df2 ;
        if (_labels.size() != 2) result = -1d; 
        else {
            String class1 = _labels.get(0);
            String class2 = _labels.get(1);
            Map<String, Integer> dfij = getDFij(term);
            df1= dfij.get(class1);
            df2 = dfij.get(class2);
            bigN1 = _labelCounts.get(df1);
            bigN2 = _labelCounts.get(df2);
            result = ((double)df2 * ((double)bigN1 - (double)df1) + 0.5) / ((double)df1 * ((double)bigN2 - (double)df2) + 0.5) ;
        }
        return Document.log2(result) ;
    }
    
    public double getDeltaBM25Idf(String term) {
        double result = 0d;
        double bigN1, bigN2, df1, df2 ;
        if (_labels.size() != 2) result = -1d; 
        else {
            String class1 = _labels.get(0);
            String class2 = _labels.get(1);
            Map<String, Integer> dfij = getDFij(term);
            df1= dfij.get(class1);
            df2 = dfij.get(class2);
            bigN1 = _labelCounts.get(df1);
            bigN2 = _labelCounts.get(df2);
            result = ((double)df2 * ((double)bigN1 - (double)df1 + 0.5) + 0.5) / ((double)df1 * ((double)bigN2 - (double)df2 + 0.5) + 0.5) ;
        }
        return Document.log2(result) ;
    }
    
    
    public double getIdf(String term) {
        int bigN = _docs.size() ;
        int smallN = _termToDocs.getOrDefault(term, new HashSet<>()).size() ;
        double idf = smallN == 0 ? 0 : Document.log2(((double)bigN/(double) smallN) );
        return idf;
    }
    
    public double getProbabilisticIdf(String term) {
        int bigN = _docs.size() ;
        int smallN = _termToDocs.getOrDefault(term, new HashSet<>()).size() ;
        double idf = bigN - smallN == 0 ? 0 : Document.log2((((double)bigN - (double)smallN)/ (double)smallN) );
        return idf;
    }
    
    public double getBM25Idf(String term) {
        int bigN = _docs.size() ;
        int smallN = _termToDocs.getOrDefault(term, new HashSet<>()).size() ;
        double idf = smallN == 0 ? 0 : Document.log2((((double)bigN - (double)smallN + 0.5)/ (double)smallN + 0.5) );
        return idf;
    }
    
    public static double tfIdf(String term, Document document, DocumentBase registry) {
        System.out.println("TF of " + term + " in " + document.getId() + " is " + document.logFrequency(term));
        System.out.println("IDF of " + term + " is " + registry.getIdf(term));
        double tfidf = document.logFrequency(term) * registry.getIdf(term) ;
        System.out.println("Therefore TFIDF(" + term + "," + document.getId()+") = " + tfidf);
        return tfidf;
    }
    public enum TermWeight {Boolean, NaturalFrequency, LogFrequency, AugmentedFrequency, LogAverageFrequency, BM25Frequency}
    public enum NormFactor {NoNormalization, IDF, ProbabilisticIDF, BM25IDF, DeltaIDF, DeltaSmoothedIDF, DeltaProbIDF, DeltaSmoothedProbIDF, DeltaBM25IDF}
  
    public static double BM25_k1 = 1.2d;
    public static double BM25_b = 0.95d;
    public static double BM25_avgDL = -1d;
    
    public void computeBM25Values() {
        int totalLength = 0 ;
        for (Document doc : _docs.values()) {
            totalLength += doc.docLength();
        }
        BM25_avgDL = (double) totalLength / (double) _docs.size() ;
    }
 
    public static double getValue(String term, Document document, DocumentBase registry, TermWeight tw, NormFactor nf) {
        double termWeight = 0d;
        double normFactor = 0d;
        NormFactor nf2 ;
        if (tw == TermWeight.Boolean) nf2 = NormFactor.NoNormalization ;
        else nf2 = nf ;
        //Term Weight
        switch(tw) {
            case Boolean:{
                if (document.rawFrequency(term) > 0) termWeight = 1 ;
                else termWeight = 0 ;
                System.out.println("Term weight (" + tw.name() + ") = " + termWeight);
                break;
            }
            case NaturalFrequency: {
                termWeight = document.rawFrequency(term);
                System.out.println("Term weight (" + tw.name() + ") = " + termWeight);
                break;
            }
            case LogFrequency: {
                termWeight = document.logFrequency(term);
                System.out.println("Term weight (" + tw.name() + ") = " + termWeight);
                break;
            }
            case AugmentedFrequency: {
                termWeight = document.augFrequency(term);
                System.out.println("Term weight (" + tw.name() + ") = " + termWeight);
                break;
            }
            case LogAverageFrequency: {
                termWeight = document.logAverageFrequency(term);
                System.out.println("Term weight (" + tw.name() + ") = " + termWeight);
                break;
            }
            case BM25Frequency: {
                if (BM25_avgDL == -1d) registry.computeBM25Values();
                termWeight = document.BM25TF(term, BM25_k1, BM25_b, BM25_avgDL);
                System.out.println("Term weight (" + tw.name() + ") = " + termWeight);
                break;
            }
        }
        //Normalization Factor
        switch (nf2) {
            case NoNormalization: {
                normFactor = 1 ;
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }
            case IDF: {
                normFactor = registry.getIdf(term);
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }
            case ProbabilisticIDF: {
                normFactor = registry.getProbabilisticIdf(term);
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }
            case BM25IDF: {
                normFactor = registry.getBM25Idf(term);
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }
            case DeltaIDF: {
                normFactor = registry.getDeltaIdf(term);
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }
            case DeltaSmoothedIDF: {
                normFactor = registry.getDeltaSmoothedIdf(term);
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }
            case DeltaProbIDF: {
                normFactor = registry.getDeltaProbIdf(term);
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }
            case DeltaSmoothedProbIDF: {
                normFactor = registry.getDeltaSmoothedProbIdf(term);
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }
            case DeltaBM25IDF: {
                normFactor = registry.getDeltaBM25Idf(term);
                System.out.println("Normalization Factor (" + nf.name() + ") = " + normFactor);
                break;
            }

        }
        return termWeight * normFactor;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.tfidf.classification;

import rgu.ac.uk.tfidf.classification.DocumentBase.NormFactor;
import rgu.ac.uk.tfidf.classification.DocumentBase.TermWeight;



/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Test {
    public static void main(String[] args) {
        Document product1 = new Document("Product1","positive");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("price");
        product1.addTerm("size");
        product1.addTerm("delivery");
        product1.addTerm("screen");
        product1.addTerm("good");
        product1.addTerm("amazing");
        product1.addTerm("ok");
        Document product2 = new Document("Product2","negative");
        product2.addTerm("price");
        product2.addTerm("delivery");
        product2.addTerm("terrible");
        product2.addTerm("bad");
        product2.addTerm("sad");
        Document product3 = new Document("Product3","positive");
        product3.addTerm("price");
        product3.addTerm("good");
        product3.addTerm("like");
        product3.addTerm("delivery");
        Document product4 = new Document("Product4","positive");
        product4.addTerm("screen");
        product4.addTerm("good");
        product4.addTerm("wellmade");
        product4.addTerm("excellent");
        Document product5 = new Document("Product5","negative");
        product5.addTerm("terrible");
        product5.addTerm("bad");
        product5.addTerm("disagree");
        product5.addTerm("dangerous");
        
        DocumentBase registry = new DocumentBase();
        registry.addDocument(product1);
        registry.addDocument(product2);
        registry.addDocument(product3);
        registry.addDocument(product4);
        registry.addDocument(product5);
     
        registry.computeLabels();
        System.out.println(DocumentBase.getValue("screen", product1, registry, TermWeight.LogFrequency, NormFactor.ProbabilisticIDF));
        System.out.println(DocumentBase.tfIdf("screen", product1, registry));
        
    }
}
